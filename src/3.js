// @flow
const { getInput } = require('./_utils');
const _ = require('lodash');

type ClaimType = string;
type InvalidClaimType = null;
interface Rect {
  width: number,
  height: number
}
interface Position {
  x: number,
  y: number
}
type PositionableRectType = Rect & Position;

const parseClaim = (claim: ClaimType): PositionableRectType | InvalidClaimType => {
  const pattern = /^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$/;
  const match = claim.match(pattern);
  if (!match) { return null; }

  return {
    id: match[1],
    x: parseInt(match[2], 10),
    y: parseInt(match[3], 10),
    width: parseInt(match[4], 10),
    height: parseInt(match[5], 10),
  };
};

const getBoundaries = (rects: PositionableRectType[]): PositionableRectType => {
  const minX = rects.reduce((min: number, rect: PositionableRectType): number => {
    const x = rect.x;
    if (min === -1) { return x; }
    if (x < min) { return x; }
    return min;
  }, -1);
  const minY = rects.reduce((min: number, rect: PositionableRectType): number => {
    const y = rect.y;
    if (min === -1) { return y; }
    if (y < min) { return y; }
    return min;
  }, -1);
  const maxX = rects.reduce((max: number, rect: PositionableRectType): number => {
    const x = rect.x + rect.width;
    if (max === -1) { return x; }
    if (x > max) { return x; }
    return max;
  }, -1);
  const maxY = rects.reduce((max: number, rect: PositionableRectType): number => {
    const y = rect.y + rect.height;
    if (max === -1) { return y; }
    if (y > max) { return y; }
    return max;
  }, -1);

  return {
    x: minX,
    y: minY,
    width: maxX - minX,
    height: maxY - minY,
  };
};

const getGrid = (x: number, y: number, defaultItem: number= 0): number[][] => {
  if (x < 1 || y < 1) { return []; }

  return Array.apply(null, { length: x }).map(() => {
    return Array.apply(null, { length: y }).map(() => defaultItem);
  })
}

const applyRectsToMap = (rects: PositionableRectType[], initialMap: number[][]): number[][] => {
  return rects.reduce((map, rect) => {
    for (let x = rect.x; x < rect.x + rect.width; x++) {
      for (let y = rect.y; y < rect.y + rect.height; y++) {
        map[x][y] += 1;
      }
    }

    return map;
  }, _.cloneDeep(initialMap));
}

const countRectIDsOnMap = (rects: PositionableRectType[], initialMap: number[][]): number[][][] => {
  return rects.reduce((map, rect) => {
    for (let x = rect.x; x < rect.x + rect.width; x++) {
      for (let y = rect.y; y < rect.y + rect.height; y++) {
        if (map[x][y] === null) {
          map[x][y] = [rect.id];
        } else {
          map[x][y].push(rect.id);
        }
      }
    }

    return map;
  }, _.cloneDeep(initialMap));
}

const detectCollisions = (hits, ids) => {
  return ids;
}

const rawInput = getInput(3);
const claims = rawInput.trim().split('\n');
const rects = claims.map(parseClaim);
const boundaries = getBoundaries(rects);

module.exports = {
  part1(): number {
    const baseMap = getGrid(
      boundaries.x + boundaries.width,
      boundaries.y + boundaries.height,
    );
    const rectsMap = applyRectsToMap(rects, baseMap);
    return _.flatten(rectsMap).filter((x: number): boolean => x >= 2).length;
  },
  part2() {
    const baseMap = getGrid(
      boundaries.x + boundaries.width,
      boundaries.y + boundaries.height,
      null
    );
    const countedMap = countRectIDsOnMap(rects, baseMap);
    const areas = rects.reduce((acc, rect) => {
      return {
        ...acc,
        [rect.id]: rect.width * rect.height
      };
    }, {})
    const singleHitCounts = _.flatten(countedMap).filter(x => x && x.length === 1);
    const singleHitsByID = singleHitCounts.reduce((acc, hit) => {
      const id = parseInt(hit[0]);
      if (!acc[id]) {
        acc[id] = 1;
      } else {
        acc[id] += 1;
      }
      return acc;
    }, {});
    return Object.entries(singleHitsByID).filter(e => {
      const [ id, count ] = e;
      return areas[id] === count;
    })[0][0];
  },
  getBoundaries,
};
