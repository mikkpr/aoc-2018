const fs = require('fs');
const path = require('path');

const getInput = (num) => {
  return fs.readFileSync(path.join(__dirname, '..', 'data', `${num}.txt`), 'utf-8');
};

module.exports = {
  getInput
}
