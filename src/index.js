// @flow
const solve1 = require('./1').part1;
const solve1extra = require('./1').part2;
const solve2 = require('./2').part1;
const solve2extra = require('./2').part2;
const solve3 = require('./3').part1;
const solve3extra = require('./3').part2;
const solve4 = require('./4').part1;
const solve4extra = require('./4').part2;

const puzzleNum: (string | null) = process.env.DAY || null;

const solvers: {[key: (string | null)]: () => string} = {
  '1': solve1,
  '1x': solve1extra,
  '2': solve2,
  '2x': solve2extra,
  '3': solve3,
  '3x': solve3extra,
  '4': solve4,
  '4x': solve4extra
};

const getSolution = (num: string): string => {
  if (solvers[num]) {
    return solvers[num]();
  }

  return 'Invalid puzzle number.';
};

const main = () => {
  if (!puzzleNum) {
    Object.keys(solvers).forEach((day: string): void => console.log(`${day}: ${getSolution(day)}`));
  } else {
    console.log(getSolution(puzzleNum));
  }
};

main();
