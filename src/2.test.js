const { getCharScore, grade, findSimilar, getMatchingChars } = require('../lib/2.js');

describe('getCharScore', () => {
  test('with valid inputs', () => {
    expect(getCharScore('a')).toEqual(1);
    expect(getCharScore('z')).toEqual(26);
  });

  test('with invalid inputs', () => {
    expect(getCharScore('A')).toEqual(0);
    expect(getCharScore('Z')).toEqual(0);
  });
});

describe('grade', () => {
  test('with valid inputs', () => {
    expect(grade('foobar')).toEqual(57);
  })
});

describe('findSimilar', () => {
  test('with valid inputs', () => {
    const inputs = [
      'abcde',
      'fghij',
      'klmno',
      'pqrst',
      'fguij',
      'axcye',
      'wvxyz'
    ];
    expect(findSimilar(inputs)).toEqual(['fguij', 'fghij']);
  })
})

describe('getMatchingChars', () => {
  test('with valid inputs', () => {
    expect(getMatchingChars('foobar', 'foobaz')).toEqual('fooba');
  })
})
