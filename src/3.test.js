const { parseClaim, intersection } = require('../lib/3.js');

describe('parseClaim', () => {
  test('with valid inputs', () => {
    const claim1 = '#1 @ 393,863: 11x29';
    const claim2 = '#2 @ 675,133: 15x26';
    expect(parseClaim(claim1)).toEqual({id: 1, x: 393, y: 863, width: 11, height: 29});
    expect(parseClaim(claim2)).toEqual({id: 2, x: 675, y: 133, width: 15, height: 26});
  })
});

describe('intersection', () => {
  test('with valid inputs', () => {
    const rect1 = {x: 1, y: 1, width: 10, height: 10};
    const rect2 = {x: 5, y: 5, width: 10, height: 10};

    expect(intersection(rect1, rect2)).toEqual({x: 5, y: 5, width: 5, height: 5});
  })
})
