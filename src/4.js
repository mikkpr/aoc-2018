const { getInput } = require('./_utils');

const rawInput = getInput(4);
const inputLines = rawInput.trim().split('\n');
const sortedLines = inputLines.sort((a, b) => a.localeCompare(b));
const patterns = {
  beginsShift: /^\[\d+-\d+-\d+ \d+:\d+\] Guard #(\d+) begins shift$/,
  fallsAsleep: /^\[\d+-\d+-\d+ \d+:(\d+)\] falls asleep$/,
  wakesUp: /^\[\d+-\d+-\d+ \d+:(\d+)\] wakes up$/,
};

const handleBeginShift = (data, match) => {
  const guardID = match[1];
  return {
    ...data,
    currentGuardID: guardID
  };
}

const handleFallsAsleep = (data, match) => {
  const minute = parseInt(match[1]);
  const currentGuardData = data.guards[data.currentGuardID] || [];

  currentGuardData.push({ start: minute, end: null });
  return {
    ...data,
    guards: {
      ...data.guards,
      [data.currentGuardID]: currentGuardData
    }
  };
}

const handleWakesUp = (data, match) => {
  const { guards, currentGuardID } = data;
  const minute = parseInt(match[1]);
  const currentGuardData = guards[currentGuardID] || [];

  currentGuardData[currentGuardData.length - 1].end = minute;

  return {
    ...data,
    guards: {
      ...guards,
      [currentGuardID]: currentGuardData
    }
  };
}

const guardSleepPatterns = sortedLines.reduce((data, line) => {
  if (line.match(patterns.beginsShift)) {
    return handleBeginShift(data, line.match(patterns.beginsShift));
  } else if (line.match(patterns.fallsAsleep)) {
    return handleFallsAsleep(data, line.match(patterns.fallsAsleep));
  } else if (line.match(patterns.wakesUp)) {
    return handleWakesUp(data, line.match(patterns.wakesUp));
  }
}, {currentGuardID: null, guards: {}}).guards;

const getTotalSleepTime = (guard) => {
  return guard.reduce((acc, { start, end }) => (acc + (end - start)), 0);
}

const mostSleepingGuardID = Object.keys(guardSleepPatterns).reduce((prevGuardID, guardID) => {
  if (prevGuardID === null) { return guardID; }

  const guard = guardSleepPatterns[guardID];
  const prevGuard = guardSleepPatterns[prevGuardID];
  const totalSlept = getTotalSleepTime(guard);
  const totalPrevSlept = getTotalSleepTime(prevGuard);

  return (totalSlept > totalPrevSlept) ? guardID : prevGuardID;
}, null);

const getMostSleptMinute = (guard) => {
  return Object.entries(guard
    .reduce((acc, pattern) => {
      const { start, end } = pattern;
      return [...Array(end - start).keys()]
        .map(n => n + start)
        .reduce((acc, minute) => {
          if (!acc[minute]) {
            acc[minute] = 1;
          } else {
            acc[minute] += 1;
          }
          return acc;
        }, acc);
    }, {}))
    .reduce((acc, minute) => {
      if (acc === null) { return minute; }

      return (minute[1] > acc[1]) ? minute : acc;
    }, null);
};


const mostSleptMinute = getMostSleptMinute(guardSleepPatterns[mostSleepingGuardID]);

module.exports = {
  part1() {
    return parseInt(mostSleptMinute, 10) * parseInt(mostSleepingGuardID, 10);
  },
  part2() {
    //return Object.keys(guardSleepPatterns).map((id) => getMostSleptMinute(id));
  },
};
