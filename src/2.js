// @flow
const { getInput } = require('./_utils');

type CharType = string;

type CharCountsType = {
  [char: CharType]: number
};

function countChars(str: string): CharCountsType {
  const chars: CharType[] = str.split('');
  return chars.reduce((acc: CharCountsType, char: CharType): CharCountsType => {
    if (acc[char]) {
      acc[char] += 1;
    } else {
      acc[char] = 1;
    }
    return acc;
  }, {});
}

function hasTwo(str: string): boolean {
  const counts = countChars(str);
  return Object.values(counts).filter(c => c === 2).length >= 1;
}

function hasThree(str: string): boolean {
  const counts = countChars(str);
  return Object.values(counts).filter(c => c === 3).length >= 1;
}

function checksum(strs: string[]): number {
  const twos = strs.filter(hasTwo).length;
  const threes = strs.filter(hasThree).length;

  return twos * threes;
}

function getCharScore(char: CharType): number {
  if (!Boolean(char.match(/[a-z]/))) { return 0; }
  const base = 'a'.charCodeAt(0);

  return char.charCodeAt(0) - base + 1;
}

function grade(str: string): number {
  if (!str) { return 0; }
  return str
    .split('')
    .map(getCharScore)
    .reduce((acc, score) => acc + score, 0);
}

function strDiff(str1: string, str2: string): number {
  const str1scores = str1.split('').map(getCharScore);
  const str2scores = str2.split('').map(getCharScore);

  return str1scores.map((score, idx) => {
    const other = str2scores[idx];
    return score !== other ? 1 : 0;
  }).reduce((acc, score) => acc + score, 0);
}

function findSimilar(strs: string[]): string[] {
  const sorted = strs.sort((a, b) => a.localeCompare(b));
  return sorted.reduce((acc, str) => {
    if (acc === null) {
      return str;
    } else if (typeof acc === 'object' && acc.length && acc.length === 2) {
      return acc;
    } else if (strDiff(acc, str) === 1) {
      return [str, acc];
    } else {
      return str;
    }
  }, null);
}

function getMatchingChars(str1: string, str2: string): string {
  const str1chars = str1.split('');
  const str2chars = str2.split('');
  return str1chars.filter((char, idx) => {
    return char === str2chars[idx];
  }).join('');
}

const rawInput = getInput(2);

module.exports = {
  part1(): number {
    const inputs = rawInput.split('\n');
    return checksum(inputs);
  },
  part2(): string {
    const inputs = rawInput.split('\n');
    return getMatchingChars(...findSimilar(inputs));
  },
  getCharScore: getCharScore,
  grade: grade,
  findSimilar: findSimilar,
  getMatchingChars: getMatchingChars
}
