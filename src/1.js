// @flow
const _ = require('lodash');
const { getInput } = require('./_utils');

const rawInput = getInput(1);

const stripLeadingWhitespace = (str: string): string => str.replace(/^\s+/, '');
const stripTrailingWhitespace = (str: string): string => str.replace(/\s+$/, '');
const splitBy =
  (char: string): ((string) => string[]) => (str: string): string[] => str.split(char);

const removePattern =
  (pattern: RegExp): ((string) => string) => (str: string): string => str.replace(pattern, '');

const normalizeInput = (input: string): number[] => _.flow(
  stripLeadingWhitespace,
  stripTrailingWhitespace,
  splitBy('\n'),
)(input).map(_.flow(
  removePattern(/\+/),
  parseInt,
));

const sum = (a: number, b: number): number => a + b;

type FrequencyType = number;

type FrequencyCountType = {
  previousFrequency: FrequencyType,
  counts: {
    [key: FrequencyType]: number
  },
  duplicate: null | FrequencyType
};

const getFirstRepeatingFrequency = (
  counts: FrequencyCountType,
  frequency: FrequencyType,
): FrequencyCountType => {
  const newCounts = _.clone(counts);
  if (newCounts.duplicate !== null) { return newCounts; }

  const currentFrequency = newCounts.previousFrequency + frequency;

  if (!newCounts.counts[currentFrequency]) {
    newCounts.counts[currentFrequency] = 1;
  } else {
    newCounts.counts[currentFrequency] += 1;
  }

  if (newCounts.counts[currentFrequency] > 1) {
    newCounts.duplicate = currentFrequency;
  }

  newCounts.previousFrequency = currentFrequency;

  return newCounts;
};

const normalizedInput = normalizeInput(rawInput);

const INITIAL_FREQUENCY = 0;

module.exports = {
  part1(): number {
    return normalizedInput.reduce(sum, INITIAL_FREQUENCY);
  },

  part2(): FrequencyType {
    function getFrequencyCounts(prevCounts = null) {
      const initial = {
        previousFrequency: INITIAL_FREQUENCY,
        counts: { '0': 1 },
        duplicate: null
      };
      const counts = normalizedInput.reduce(getFirstRepeatingFrequency, prevCounts === null ? initial : prevCounts);
      if (counts.duplicate) {
        return counts.duplicate;
      } else {
        return getFrequencyCounts(counts);
      }
    }

    return getFrequencyCounts();
  },
};
